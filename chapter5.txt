#### Chapter 5, Manipulating database using SQL, Advanced ####

In this chapter, we learn about the grammar especially used frequently in various grammars of SQL which is used for database manipulation.

### 5.1 AND/OR operator ###

In conditions, such as the WHERE clause specified in the SELECT statement, the operators AND and OR can be used to specify two or more conditions. The results of the SQL statement will be displayed if both conditions in the AND operator are true, or either conditions in the OR operator is true.

The following example searches row data that the price column value is grater than 50 and smaller than 100 from the prod table.

The following example searches row data that the customer_id column value is 1 or 2 from the customer table.

### 5.2 LIKE operator ###

It retrieves row data that the certain column value coincides with the specified condition.

Table 5.1 The wild card which can be used in the LIKE operator

  _  One character
  %  The string of zero or more characters

The following example searches row data that the customer_name column value starts with  "T" from the customer table.

The following example searches row data that the customer_name column value includes   "d" from the customer table. Since "%" is specified backward and forward, the search condition will be fulfilled if somewhere in the value have "d". 

Although the LIKE operator is useful, you need to use it carefully since search performance may become late.
